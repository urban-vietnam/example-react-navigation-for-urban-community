import * as React from 'react';
import { View, Text } from 'react-native';
import { MessagesStackScreenProps } from '../types';

export default function Screen(props: MessagesStackScreenProps<'Message'>) {
  const { route: {params: {roomId}} } = props;

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>{`${roomId} Room`}</Text>
    </View>
  );
}
