import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { AfterLoginTabScreenProps } from '../types';

export default function Screen({navigation}: AfterLoginTabScreenProps<'Messages'>) {

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Messages</Text>
      <Button title='to Room →' onPress={() => navigation.navigate('Message', {roomId: 'TESTRoom'})} />
    </View>
  );
}
