import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { BeforeLoginStackScreenProps } from '../types';

export default function Screen({navigation}: BeforeLoginStackScreenProps<'SignIn'>) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Sign in</Text>
      <Button title='to Help →' onPress={() => navigation.navigate('Help')} />
    </View>
  );
}
