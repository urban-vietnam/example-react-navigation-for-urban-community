/***************************************************************
 * Custum my drawer content
 ***************************************************************/
/** import react native */
import React from 'react';
/** import parts */
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
/** import type */
import { DrawerContentComponentProps } from '@react-navigation/drawer';

/***************************************************************
 * render
 ***************************************************************/
/** export my Drawer */
export default function CustomDrawerContent(props: DrawerContentComponentProps) {
  return (
    // render my drawer
    <DrawerContentScrollView {...props}>
      {/* change screen menues */}
      <DrawerItem label='Home' onPress={() => {props.navigation.navigate('Messages')}}/>
      <DrawerItem label='Your Profile' onPress={() => {props.navigation.navigate('Profile')}}/>
      <DrawerItem label='Contact' onPress={() => {props.navigation.navigate('Contacts')}}/>
      <DrawerItem label='Search Friend' onPress={() => {props.navigation.navigate('SearchFriend')}}/>
      <DrawerItem label='Change Password' onPress={() => {props.navigation.navigate('ChangePassword')}}/>
      <DrawerItem label='Advanced Settings' onPress={() => {props.navigation.navigate('Settings')}}/>
      {/* Sign out menu TODO: function call signout */}
      <DrawerItem label='Sign Out' onPress={() => {}}/>
      <DrawerItem label='About us' onPress={() => {props.navigation.navigate('Aboutus')}}/>
      <DrawerItem label='Help' onPress={() => {props.navigation.navigate('Help')}}/>
    </DrawerContentScrollView>
  );
}
