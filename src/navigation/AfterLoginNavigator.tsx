/***************************************************************
 * AfterLogin root navigation
 ***************************************************************/
/** import react native */
import React from 'react';
/** import react navigation */
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
/** import parts */
import DrawerContent from '../components/AfterLoginDrawerContent';
/** import screen */
import MessagesScreen from '../screens/MessagesScreen';
import ContactsScreen from '../screens/ContactsScreen';
import SettingsScreen from '../screens/SettingsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SearchFriendScreen from '../screens/SearchFriendScreen';
import ChangePasswordScreen from '../screens/ChangePasswordScreen';
import HelpScreen from '../screens/HelpScreen';
import AboutusScreen from '../screens/AboutusScreen';
import RoomScreen from '../screens/RoomScreen';
/** import type */
import { DrawerContentComponentProps} from '@react-navigation/drawer';
import { AfterLoginStackParamList, AfterLoginDrawerParamList, AfterLoginTabParamList } from '../types';

/***************************************************************
 * render
 ***************************************************************/
// AfterLogin stack navigator
const Stack = createNativeStackNavigator<AfterLoginStackParamList>();
// AfterLogin Drawer navigator
const Drawer = createDrawerNavigator<AfterLoginDrawerParamList>();
// AfterLogin Bottom tab navigator
const Tab = createBottomTabNavigator<AfterLoginTabParamList>();

/** export navigation */
/**
 * Render tab navigation = need bottom tab navigation screens
 */
const TabNavigation = () => {
  return (
    // Bottom ○
    <Tab.Navigator initialRouteName='Messages'>
      <Tab.Screen name='Messages' component={MessagesScreen} />
      <Tab.Screen name='Contacts' component={ContactsScreen} />
      <Tab.Screen name='Settings' component={SettingsScreen} />
    </Tab.Navigator>
  );
}
/**
 * Render navigation = need dawer header screens navigation
 */
const DrawerNavigation = () => {
  return (
    <Drawer.Navigator initialRouteName='Home' drawerContent={(props: DrawerContentComponentProps) => <DrawerContent {...props} />}>
      {/* Drawer ○, Bottom ○(need side menu bar, bottom tab) */}
      <Drawer.Screen name='Home' component={TabNavigation} />
      {/* Drawer ○, Bottom ×(need side menu bar) */}
      <Drawer.Screen name='Profile' component={ProfileScreen} />
      <Drawer.Screen name='SearchFriend' component={SearchFriendScreen} />
      <Drawer.Screen name='ChangePassword' component={ChangePasswordScreen} />
    </Drawer.Navigator>
  );
};
/** export navigation */
export default function Navigation() {
  return (
    <Stack.Navigator initialRouteName='AfterLoginDrawer'>
      {/* Drawer ○(need side menu bar) */}
      <Stack.Screen name='AfterLoginDrawer' component={DrawerNavigation} />
      {/* Drawer ×(not) */}
      <Stack.Screen name='Help' component={HelpScreen} />
      <Stack.Screen name='Aboutus' component={AboutusScreen} />
      <Stack.Screen name='Message' component={RoomScreen} />
    </Stack.Navigator>
  );
}

