/***************************************************************
 * root navigation this applicatation
 ***************************************************************/
/** import react native */
import React from 'react';
import { ColorSchemeName } from 'react-native';
/** import react navigation */
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
/** import my navigation */
import AfterLoginNavigator from './AfterLoginNavigator';
import BeforeLoginNavigator from './BeforeLoginNavigator';
/** import screen */
import HelpScreen from '../screens/HelpScreen';
import AboutusScreen from '../screens/AboutusScreen';
/** import type */
import { RootStackParamList } from '../types';

/***************************************************************
 * render root routes
 ***************************************************************/
// A root stack navigator
const Stack = createNativeStackNavigator<RootStackParamList>();

// render root navigation
const RootNavigator = () => {
  // auth -> useQuery(GET_ME)
  const data = { getMe: { user: { nickname: 'yhigu'}, token: 'abcdefg'} }; // getMe response example
  const loading = false; // of getMe request
  // render = complete request query
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {
        // ログイン状態別に 主な ナビゲーションを変更
        (data?.getMe && !loading) // has token && get user: token is valid
        ? (<Stack.Screen name='AfterLogin' component={AfterLoginNavigator} />) // ログイン済 => After
        : (<Stack.Screen name='BeforeLogin' component={BeforeLoginNavigator} />) // ログイン前 => Before
      }
      {/* common screen = Help ... etc */}
      <Stack.Screen name='Help' component={HelpScreen} />
      <Stack.Screen name='Aboutus' component={AboutusScreen} />
    </Stack.Navigator>
  );
}

/** export root navigation */
export default function RootNavigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    // [Root navigator] is used for screen transitions. NavigationContainer is wrapper.
    // <NavigationContainer theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
    <NavigationContainer theme={colorScheme === 'dark' ? DefaultTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}
