/***************************************************************
 * BeforeLogin root navigation
 ***************************************************************/
/** import react native */
import React from 'react';
/** import react navigation */
import { createNativeStackNavigator } from '@react-navigation/native-stack';
/** import screen */
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
/** import type */
import { BeforeLoginStackParamList } from '../types';
/***************************************************************
 * render
 ***************************************************************/
// BeforeLogin stack navigator
const Stack = createNativeStackNavigator<BeforeLoginStackParamList>();

/** export navigation */
export default function Navigation() {
  return (
    <Stack.Navigator initialRouteName='SignIn'>
      <Stack.Screen name='SignIn' component={SignInScreen} />
      <Stack.Screen name='SignUp' component={SignUpScreen} />
    </Stack.Navigator>
  );
}

