/** import react navigation types */
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { CompositeScreenProps, NavigatorScreenParams } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { DrawerScreenProps } from '@react-navigation/drawer';

/** entry point */
export type RootStackParamList = {
  BeforeLogin: NavigatorScreenParams<BeforeLoginStackParamList> | undefined;
  AfterLogin: NavigatorScreenParams<AfterLoginStackParamList> | undefined;
  Help: undefined;
  Aboutus: undefined;
};

/** for Before Login stack navigation */
export type BeforeLoginStackParamList = {
  SignUp: undefined;
  SignIn: undefined;
};
export type BeforeLoginStackScreenProps<Screen extends keyof BeforeLoginStackParamList> = CompositeScreenProps<
  DrawerScreenProps<BeforeLoginStackParamList, Screen>,
  NativeStackScreenProps<RootStackParamList>
>;

/** for Before Login stack navigation */
export type AfterLoginStackParamList = {
  // drawer menu screens
  AfterLoginDrawer: NavigatorScreenParams<AfterLoginDrawerParamList> | undefined;
  // AfterLogin Stack screens
  Message: {roomId: string};
  Help: undefined;
  Aboutus: undefined;
};
export type AfterLoginStackScreenProps<Screen extends keyof AfterLoginStackParamList> = CompositeScreenProps<
  DrawerScreenProps<AfterLoginStackParamList, Screen>,
  NativeStackScreenProps<RootStackParamList>
>;

/** for AfterLogin drawer menu */
export type AfterLoginDrawerParamList = {
  Home: NavigatorScreenParams<AfterLoginTabParamList> | undefined // Home = Messages (initial) and render TabScreens
  Profile: undefined
  SearchFriend: undefined
  ChangePassword: undefined
}
export type AfterLoginDrawerScreenProps<Screen extends keyof AfterLoginDrawerParamList> = CompositeScreenProps<
  DrawerScreenProps<AfterLoginDrawerParamList, Screen>,
  NativeStackScreenProps<AfterLoginStackParamList>
>;

/** for AfterLogin bottom tab menu */
export type AfterLoginTabParamList = {
  Messages: undefined;
  Contacts: undefined;
  Settings: undefined;
};
export type AfterLoginTabScreenProps<Screen extends keyof AfterLoginTabParamList> = CompositeScreenProps<
  BottomTabScreenProps<AfterLoginTabParamList, Screen>,
  NativeStackScreenProps<AfterLoginStackParamList>
>;
